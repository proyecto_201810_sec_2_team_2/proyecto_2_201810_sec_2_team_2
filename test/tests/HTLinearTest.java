package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Cola;
import model.data_structures.HashTableLinearProbing;
import model.vo.Taxi;

public class HTLinearTest <Key, Value>{

	private HashTableLinearProbing<Key, Value> ht;
	Value taxi = (Value) new Taxi("id", "compania");
	Key llave = (Key) "llave";
	Value taxi2 = (Value) new Taxi("id2", "compania2");
	Key llave2 = (Key) "llave2";

	@Before
	public void setup1()
	{
		ht = new HashTableLinearProbing<>();
	}


	@Test
	public void testPut()
	{
		setup1();
		ht.put(llave, taxi);
		assertEquals(1, ht.giveEntries());
	}

	@Test
	public void testGet()
	{
		setup1();
		ht.put(llave, taxi);	
		assertEquals(taxi, ht.get(llave));
	}
	
	@Test
	public void testDelete()
	{
		setup1();

		ht.put(llave, taxi);
		ht.delete(llave);
		assertEquals(0, ht.giveEntries());
	}
	
	@Test
	public void testReHash()
	{
		setup1();
		ht.put(llave, taxi);
		ht.put(llave2, taxi2);
		ht.resize(8);
		System.out.println(ht.giveCapacity());
		assertEquals(taxi2, ht.get(llave2));
		assertEquals(11, ht.giveCapacity());//11 porque es el primo siguiente
	}
	
	@Test
	public void testIter()
	{
		setup1();

		ht.put(llave, taxi);
		ht.put(llave2, taxi2);
		Iterator iter = ht.keys();
		assertEquals(llave, iter.next());
		assertEquals(llave2, iter.next());

	}
}

