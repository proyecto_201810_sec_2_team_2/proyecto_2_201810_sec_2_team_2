package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.Cola;
import model.data_structures.Pila;
import model.vo.Taxi;


public class PilaTest <T extends Comparable <T>> {

	private Pila pila;

	Taxi taxi1 = new Taxi("id", "compania");

	Taxi taxi2 = new Taxi("i2", "compania2");

	@Before
	public void setup1()
	{
		pila = new Pila();
	}

	@Test
	public void testPush()
	{
		setup1();
		
		pila.push(taxi1);
		pila.push(taxi2);
		
		assertEquals(2, pila.size());
	}
	
	public void testPop()
	{
		setup1();
		
		pila.push(taxi1);
		pila.push(taxi2);
		
		assertEquals(taxi2, pila.pop());
		
		
		
	}
}
