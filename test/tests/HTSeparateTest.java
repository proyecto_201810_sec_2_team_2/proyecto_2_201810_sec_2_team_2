package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.HashTableSeparateChaining;
import model.vo.Taxi;

public class HTSeparateTest<Key, Value>{

	private HashTableSeparateChaining<Key, Value> h;
	

	@Before
	public HashTableSeparateChaining<Integer, Integer> setup1()
	{
		HashTableSeparateChaining<Integer, Integer> ht = new HashTableSeparateChaining<Integer, Integer>();
		return ht ;
	}


	@Test
	public void testPut()
	{
		HashTableSeparateChaining<Integer, Integer> h = setup1();
		h.put(10, 8);
		assertTrue(h.get(10)==8);
	}

	@Test
	public void testGet()
	{
		HashTableSeparateChaining<Integer, Integer> h = setup1();
		h.put(10, 8);
		h.put(6, 8);
		assertTrue(h.get(10)==8);
		assertTrue(h.get(6)==8);
	}
	
	@Test
	public void testDelete()
	{
		HashTableSeparateChaining<Integer, Integer> h = setup1();
		h.put(10, 8);
		h.put(6, 8);
		h.delete(10);
		assertTrue(h.get(10)==null);
	}
	
	
}

