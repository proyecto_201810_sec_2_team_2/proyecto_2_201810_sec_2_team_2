package tests;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.BST;
import model.data_structures.HashTableLinearProbing;
import model.vo.Taxi;

public class BSTtest <Key extends Comparable<Key>, Value>{

	private BST<Key, Value> bst;
	Value taxi = (Value) new Taxi("id", "compania");
	Key llave = (Key) "llave";
	Value taxi2 = (Value) new Taxi("id2", "compania2");
	Key llave2 = (Key) "llave2";
	Value taxi3 = (Value) new Taxi("id3", "compania3");
	Key llave3 = (Key) "llave3";
	
	@Before
	public void setup1()
	{
		bst = new BST<>();
	}
	
	@Test
	public void testPut()
	{
		setup1();
		bst.put(llave, taxi);
		bst.put(llave2, taxi2);
		assertEquals(2, bst.size());
	}
	
	@Test
	public void testGet()
	{
		setup1();
		bst.put(llave, taxi);	
		bst.put(llave2, taxi2);
		assertEquals(taxi, bst.get(llave));
	}
	
	@Test
	public void testDelete()
	{
		setup1();

		bst.put(llave, taxi);
		bst.put(llave2, taxi2);
		bst.delete(llave);
		assertEquals(1, bst.size());
	}
	
	@Test
	public void testDeleteMax()
	{
		setup1();

		bst.put(llave, taxi);
		bst.put(llave2, taxi2);
		bst.put(llave3, taxi3);
		bst.deleteMax();
		assertEquals(null , bst.get(llave3));
	}
	
	@Test
	public void testDeleteMin()
	{
		setup1();

		bst.put(llave, taxi);
		bst.put(llave2, taxi2);
		bst.put(llave3, taxi3);
		bst.deleteMin();
		assertEquals(null, bst.get(llave));
		
		
	}
	
	
	@Test
	public void testIter()
	{
		setup1();

		bst.put(llave, taxi);
		bst.put(llave2, taxi2);
		Iterable iter = bst.keys();
		Iterator iter2 = iter.iterator();
		assertEquals(llave, iter2.next());
		assertEquals(llave2, iter2.next());

	}
	@Test
	public void testHeigth()
	{
		setup1();

		bst.put(llave, taxi);
		bst.put(llave2, taxi2);
		bst.put(llave3, taxi3);
		
		assertEquals(bst.height(), 2);
	}
	
	
	
	@Test
	public void testContains ()
	{
		setup1();

		bst.put(llave, taxi);
		assertTrue("Debio encontrar el taxi.", bst.contains(llave));
		
	}
	
	@Test
	public void testEmpty()
	{
		setup1();
		assertTrue("Debio haber estado vacia.", bst.isEmpty());
	}
	
	@Test
	public void testSize()
	{
		setup1();

		bst.put(llave, taxi);
		bst.put(llave2, taxi2);
		bst.delete(llave);
		assertEquals(1, bst.size());
		
		
	}
}
