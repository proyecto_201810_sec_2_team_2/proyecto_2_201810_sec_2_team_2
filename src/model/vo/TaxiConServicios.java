package model.vo;

import model.data_structures.Lista;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

	private String taxiId;
	private String compania;
	private Lista<Service> servicios;
	private double puntos;

	public TaxiConServicios(String taxiId, String compania){
		this.taxiId = taxiId;
		this.compania = compania;
		this.servicios = new Lista<Service>(); // inicializar la lista de servicios 
	}

	public String getTaxiId() {
		return taxiId;
	}

	public String getCompania() {
		return compania;
	}

	public Lista<Service> getServicios()
	{
		return servicios;
	}

	public int numeroServicios(){
		return servicios.size();
	}

	public void agregarServicio(Service servicio){
		servicios.add(servicio);
		puntos = getPoints();
	}

	public double getPoints()
	{
		double rta = 0;
		double millas = 0;
		double dinero = 0;
		int numeroServicios = 0;
		for (Service servicio: servicios)
		{
			if(servicio.getTripMiles()>0 && servicio.getTripTotalCost()>0) 
			{
				millas += servicio.getTripMiles();
				dinero += servicio.getTripTotalCost();
				numeroServicios ++;
			}
		}

		if(millas != 0)
		{
			double division = dinero/millas;
			rta = division * numeroServicios;
		}
		
		puntos = rta;
		return rta;
	}

	@Override
	public int compareTo(TaxiConServicios o) {
		String puntos1 = String.valueOf(puntos);
		String puntos2 = String.valueOf(o.getPoints());
//		puntos1.compareToIgnoreCase(puntos2);
		int compare = Double.compare(puntos, o.getPoints());
		return compare;
	}

	public void print(){
		System.out.println(Integer.toString(numeroServicios())+" servicios "+" Taxi: "+taxiId);
		for(Service s : servicios){
			System.out.println("\t"+s.getTripStart());
		}
		System.out.println("___________________________________");;
	}
}
