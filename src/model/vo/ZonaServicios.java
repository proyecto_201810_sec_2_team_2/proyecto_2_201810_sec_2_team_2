package model.vo;

import java.awt.List;

import model.data_structures.LinkedList;
import model.data_structures.Lista;

public class ZonaServicios implements Comparable<ZonaServicios>{

	private String idZona;
	
	private LinkedList<FechaServicios> fechasServicios;
	
	public ZonaServicios (String id,FechaServicios f)
	{
		idZona = id;
		fechasServicios = new Lista<FechaServicios>();
		fechasServicios.add(f);
	}
	
	public ZonaServicios (String id,LinkedList<FechaServicios>  f)
	{
		idZona = id;
		fechasServicios = f;
	}
	
	

	public String getIdZona() {
		return idZona;
	}



	public void setIdZona(String idZona) {
		this.idZona = idZona;
	}



	public LinkedList<FechaServicios> getFechasServicios() {
		return fechasServicios;
	}



	public void setFechasServicios(LinkedList<FechaServicios> fechasServicios) {
		this.fechasServicios = fechasServicios;
	}



	@Override
	public int compareTo(ZonaServicios o) {
		// TODO Auto-generated method stub
		return idZona.compareToIgnoreCase(o.idZona);
	}
}
