package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.Queue;
import model.data_structures.Queue2;
import model.data_structures.Stack;
import model.data_structures.BST;
import model.data_structures.BinaryHeap;
import model.data_structures.Cola;
import model.data_structures.HashTableSeparateChaining;
import model.data_structures.HeapSort;
import model.data_structures.Lista;
import model.data_structures.LinkedList;
import model.data_structures.MaxPQ;
import model.data_structures.MergeSort;
import model.data_structures.Pila;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoDuracion;
import model.vo.RangoFechaHora;
import model.vo.Service;
import model.vo.ServicioResumen;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";

	Cola <Service> colaJson = new Cola <Service>();

	Pila <Service> pilaJson = new Pila <Service>();

	Lista <Service> listaJson = new Lista <Service>();

	Lista <String> listaCom = new Lista <String>();

	Lista <String> listaZonas = new Lista <String>();
	
	Lista<String> listaDistancias = new Lista<String>(); 

	Lista<RangoDuracion> listaRango = new Lista <RangoDuracion>();

	BST<String, Lista<String>> bst1A = new BST<>();
	BST<String, Lista<String>> bst1B = new BST<>();
	BST <Double, FechaServicios> bst3C = new BST<>();
	
	int cantLat =0;
	int cantLon =0;
	
	double totalLat =0.0 ;
	double totalLon = 0.0;
	
	double lat = 0.0;
	double lon =0.0;
	

	//	BST

	HashTableSeparateChaining <Integer, Lista<Service>> htA2 = new HashTableSeparateChaining<>();

	Lista<Integer> listaArea = new Lista <Integer>();

	Lista <Integer> listaDistancia = new Lista <Integer>();
	int max = 61;
	int sysout2 = 0;

	HashTableSeparateChaining<Integer, Lista <Service>> htSepA1 = new HashTableSeparateChaining<>();
	HashTableSeparateChaining<Integer, Lista <Service>> htDis = new HashTableSeparateChaining<>();
	HashTableSeparateChaining<String,  Lista <Service>> htSep2B = new HashTableSeparateChaining<>();

	HashTableSeparateChaining<String ,Lista <Service> > htSepC2 = new HashTableSeparateChaining<>();


	@Override //1C
	public boolean cargarSistema(String direccionJson) 
	{
		boolean rta = false;

		JsonParser parser = new JsonParser();
		int max = 61;

		if (direccionJson.equals(DIRECCION_LARGE_JSON))
		{
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-08-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-07-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-06-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-05-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-04-02-2017.json");
			cargarSistema ("./data/taxi-trips-wrvz-psew-subset-03-02-2017.json");

		}

		try {
			String taxiTripsDatos = "./data/taxi-trips-wrvz-psew-subset-small.json";

			if(direccionJson.equals(DIRECCION_LARGE_JSON))
			{
				direccionJson = "./data/taxi-trips-wrvz-psew-subset-02-02-2017.json";
			}

			System.out.println("Inside loadServices with " + direccionJson);
			/* Cargar todos los JsonObject (servicio) definidos en un JsonArray en el archivo */
			JsonArray arr= (JsonArray) parser.parse(new FileReader(direccionJson));


			/* Tratar cada JsonObject (Servicio taxi) del JsonArray */
			for (int i = 0; arr != null && i < arr.size(); i++)
			{
				JsonObject obj= (JsonObject) arr.get(i);

				if(obj == null )
				{
					System.out.println("El objeto es nulo en: " + i);
				}

				/* Mostrar un JsonObject (Servicio taxi) */
				//System.out.println("------------------------------------------------------------------------------------------------");

				/* Obtener la propiedad company de un servicio (String) */
				String company = "NaN";
				if ( obj.get("company") != null)
				{ company = obj.get("company").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff_centroid_longitude de un servicio (String) */
				String dropoff_centroid_longitude = "NaN";
				if ( obj.get("dropoff_centroid_longitude") != null )
				{ dropoff_centroid_longitude = obj.get("dropoff_centroid_longitude").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff_centroid_latitude de un servicio (String)*/
				String dropoff_centroid_latitude = "NaN";
				if ( obj.get("dropoff_centroid_latitude") != null )
				{ dropoff_centroid_latitude = obj.get("dropoff_centroid_latitude").getAsString(); }
				
				double latInicio= 0;
				if ( obj.get("pickup_centroid_latitude") != null)
				{ latInicio = obj.get("pickup_centroid_latitude").getAsDouble(); }
				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				double lonInicio= 0;
				if ( obj.get("pickup_centroid_longitude") != null)
				{ lonInicio = obj.get("pickup_centroid_longitude").getAsDouble();}
				


				/* Obtener la propiedad trip ID de un servicio (String) */
				String tripId = "NaN";
				if ( obj.get("trip_id") != null)
				{ tripId = obj.get("trip_id").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip seconds de un servicio (int) */
				int tripSec = 0;
				if ( obj.get("trip_seconds") != null)
				{ tripSec = obj.get("trip_seconds").getAsInt(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip miles de un servicio (double) */
				double tripMil= 0;
				if ( obj.get("trip_miles") != null)
				{ tripMil = obj.get("trip_miles").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad trip total de un servicio (double) */
				double tripTot= 0;
				if ( obj.get("trip_total") != null)
				{ tripTot = obj.get("trip_total").getAsDouble(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad taxi ID de un servicio (String) */
				String taxiId = "NaN";
				if ( obj.get("taxi_id") != null)
				{ taxiId = obj.get("taxi_id").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad dropoff community area de un servicio (int) */
				int comArea = 0;
				if ( obj.get("dropoff_community_area") != null)
				{ comArea = obj.get("dropoff_community_area").getAsInt(); }

				/* Obtener la propiedad pickup_community_area de un servicio (String) */
				int pickup_community_area= 0;
				if ( obj.get("pickup_community_area") != null)
				{ pickup_community_area = obj.get("pickup_community_area").getAsInt(); }

				/* Obtener la propiedad trip_end_timestamp de un servicio (String) */
				String trip_end_timestamp= "NaN";
				if ( obj.get("trip_end_timestamp") != null)
				{ trip_end_timestamp = obj.get("trip_end_timestamp").getAsString(); }

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad  trip_start_timestamp de un servicio (String) */
				String  trip_start_timestamp= "NaN";
				if ( obj.get("trip_start_timestamp") != null)
				{  trip_start_timestamp = obj.get("trip_start_timestamp").getAsString(); }


				JsonObject dropoff_localization_obj = null; 


				/* Obtener la propiedad dropoff_centroid_location (JsonObject) de un servicio*/
				if ( obj.get("dropoff_centroid_location") != null )
				{ dropoff_localization_obj =obj.get("dropoff_centroid_location").getAsJsonObject();

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				/* Obtener la propiedad coordinates (JsonArray) de la propiedad dropoff_centroid_location (JsonObject)*/
				JsonArray dropoff_localization_arr = dropoff_localization_obj.get("coordinates").getAsJsonArray();

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				
				/* Obtener cada coordenada del JsonArray como Double */
				double longitude = dropoff_localization_arr.get(0).getAsDouble();
				double latitude = dropoff_localization_arr.get(1).getAsDouble();}
				//System.out.println( "[Lon: " + longitude +", Lat:" + latitude + " ] (Datos double)");

				//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				Service servicio = new Service(tripId, tripSec, tripMil, tripTot, comArea,pickup_community_area, taxiId, company, trip_start_timestamp, trip_end_timestamp,latInicio,lonInicio);

				if(listaRango.size()==0)
				{
					listaRango.add(new RangoDuracion(String.valueOf(0), String.valueOf(0)));
					listaRango.add(new RangoDuracion(String.valueOf(1), String.valueOf(60)));
				}
				boolean encontro = false;
				while(!encontro)
				{
					for(RangoDuracion rango: listaRango)
					{
						if (rango.estaDentro(String.valueOf(tripSec)))
						{encontro = true;
						rango.agregarServicio(servicio);}
					}
					if(!encontro)
					{
						listaRango.add(new RangoDuracion(String.valueOf(max), String.valueOf(max + 59)));
						max += 60;
//						System.out.println("Max: " + max);
					}
				}
				listaJson.add(servicio);
				//				System.out.println(sysout2);
//				System.out.println("Lista de rangos: " + listaRango.size());
				sysout2++;
			}

			System.out.println("En esta lista hay: " + listaJson.size());

			if(listaJson.size()>0)
			{rta = true;}

			crearArbol1A();
			System.out.println("Arbol creado");
			crearHT1A();
			int num = 0;
			for (RangoDuracion rangoActual: listaRango)
			{
				if(rangoActual.getLista().size()>0 && Integer.parseInt(rangoActual.getInicio())>0)
				{
//					System.out.println( "Inicio " + rangoActual.getInicio()+":  " +rangoActual.getLista().size() + " i: " + num);

					Lista<Service> lista = rangoActual.getLista(); 
					htA2.put(num, lista);

				}
				num++;
			}
			serviciosPorDis();
			
			tabla2B();
			System.out.println("Table 2B creado");
			lat = totalLat / cantLat;
			lon = totalLon / cantLon;
			tabla2C();
			
			System.out.println("Table 2c creado");
			
			
		}

		catch (JsonIOException e1 ) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		catch (FileNotFoundException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		return rta;
	}
	public void crearArbol1A()
	{
		int i =0;
		//		System.out.println("Lista com inicial " + listaCom.size());
		for (Service servicio : listaJson)
		{
			//			System.out.println("Servicio " + i);
			String compania = servicio.getTaxi().getCompany();
			i++;
			if (listaCom.size()==0)
			{listaCom.add(compania);}
			else
			{
				Boolean encontro = false;

				for (String com: listaCom)
				{if(com.equalsIgnoreCase(compania))
				{encontro = true;}
				}

				if(!encontro)
				{listaCom.add(compania);
				//				System.out.println("Compania anadida: " + compania);
				}
			}
		}

		for (String com: listaCom)
		{
			//			System.out.println("tama�o compa�ias " + listaCom.size());
			Lista<String> listaAux = new Lista<String>();
			for (Service actual : listaJson)
			{
				String id = actual.getTaxiId();
				if(actual.getTaxi().getCompany().equals(com))
				{
					if(listaAux.size() == 0)
					{listaAux.add(id);}
					else
					{
						Boolean encontro = false;
						for (String taxiId: listaAux)
						{if(taxiId.equals(id))
						{encontro = true;}
						}
						if(!encontro)
						{listaAux.add(id);}
					}
				}
			}
			if(com.equalsIgnoreCase("NaN"))
			{
				com = "Independent Owner";
			}
			bst1A.put(com, listaAux);	
		}
	}

	public void crearHT1A()
	{
		for (Service servicio : listaJson)
		{
			int area = servicio.getPickupCommunityArea();

			if (listaArea.size()==0)
			{listaArea.add(area);}
			else
			{
				Boolean encontro = false;

				for (int ar: listaArea)
				{
					if(ar==area)
					{
						encontro = true;
					}
				}

				if(!encontro)
				{
					listaArea.add(area);
				}
			}
		}

		for (int cod: listaArea)
		{
			Lista<Service> listaAux = new Lista<>();
			Lista<Service> listaFin = new Lista<>();
			for (Service actual : listaJson)
			{
				if(actual.getPickupCommunityArea() == cod)
				{
					listaAux.add(actual);
				}
			}
			Service[] arreglo = new Service[listaAux.size()];
			int i = 0;
			for(Service servicioActual : listaAux)
			{
				arreglo[i] = servicioActual;
				i++;
			}
			MergeSort.sort(arreglo);
			for (int j = 0; j < arreglo.length; j++)
			{
				listaFin.add(arreglo[j]);
			}
			htSepA1.put(cod, listaFin);
		}
	}




	//	Lista<Service> listaAux = new Lista<>();
	//	Lista<Service> listaServicios = new Lista<Service>();
	//	for (Service servicio : listaJson)
	//	{
	//		if(servicio.getPickupCommunityArea() == area)
	//		{
	//			listaServicios.add(servicio);
	//		}
	//	}
	//
	//	Service[] arreglo = new Service[listaServicios.size()];
	//	int i = 0;
	//	for (Service servicioActual: listaServicios)
	//	{
	//		arreglo[i] = servicioActual;
	//		i++;
	//	}
	//
	//	MergeSort.sort(arreglo);
	//	System.out.println("Numero de servicios: " + arreglo.length);
	//
	//	for (int j = 0; j < arreglo.length; j++)
	//	{
	//		Service servicio = arreglo[j];
	//		listaAux.add(servicio);
	//	}
	//	htSep.put(area, listaAux);
	//	return htSep;



	public void serviciosPorDis()
	{

		for (Service servicio : listaJson)
		{
			double distancia = servicio.getTripMiles();
			int llave = (int) Math.ceil(distancia);

			if (listaDistancia.size()==0)
			{listaDistancia.add(llave);}
			else
			{
				Boolean encontro = false;

				for (int d: listaDistancia)
				{
					if(d==llave)
					{
						encontro = true;
					}
				}

				if(!encontro)
				{
					listaDistancia.add(llave);
				}
			}
		}
		for (int cod: listaDistancia)
		{
			Lista<Service> listaAux = new Lista<>();
			for (Service actual : listaJson)
			{
				int llave = (int) Math.ceil(actual.getTripMiles());
				if(llave == cod)
				{
					listaAux.add(actual);
				}
			}

			htDis.put(cod, listaAux);
		}

	}
	public void tabla2B()
	{

		for (Service servicio : listaJson)
		{
			String llave = servicio.getPickupCommunityArea()+"-"+servicio.getDropComArea();
			if (servicio.getPickupLatitud()!=0 &&servicio.getPickupLongitud()!=0)
			{
				cantLat++;
				totalLat += servicio.getPickupLatitud();
	
				cantLon++;
				totalLon += servicio.getPickupLongitud();
			}
				
				
			if (listaZonas.size()==0)
			{listaZonas.add(llave);}
			else
			{
				Boolean encontro = false;

				for (String d: listaZonas)
				{
					if(d.equalsIgnoreCase(llave))
					{
						encontro = true;
					}
				}

				if(!encontro)
				{
					listaZonas.add(llave);
				}
			}
		}
		for (String cod: listaZonas)
		{
			Lista <Service> aux = new Lista<>();
			for (Service actual : listaJson)
			{
				String llave = actual.getPickupCommunityArea()+"-"+actual.getDropComArea();
				if(llave.equalsIgnoreCase(cod) )
				{
					aux.add( actual);
				}
			}

			htSep2B.put(cod, aux);
		}

	}
	public double toRad(double value)
	{
		 return value * Math.PI / 180;
	}
	public double getDistance ( double lat1, double lon1, double lat2, double lon2 )
	{
		final int R = 6371*1000;
		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a =  Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2* Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double d =R*c;
				return d/1609.344;
		
	}
	public double distanciaRefe ( double lat1, double lon1)
	{
		return getDistance(lat, lon, lat1, lon1);
	}
	public void tabla2C ()
	{
		for (Service servicio : listaJson)
		{
			double number =getDistance(lat, lon, servicio.getPickupLatitud(), servicio.getPickupLongitud());
//			System.out.println(number);
			
			 DecimalFormat df = new DecimalFormat("#.0");
			 
			String llave =df.format(number);
//			System.out.println(llave);
//			System.out.println(servicio.getTaxiId());
		
				
			if (listaDistancias.size()==0)
			{listaDistancias.add(llave);}
			else
			{
				Boolean encontro = false;

				for (String d: listaDistancias)
				{
					if(d.equalsIgnoreCase(llave))
					{
						encontro = true;
					}
				}

				if(!encontro)
				{
					listaDistancias.add(llave);
				}
			}
		}
		for (String cod: listaDistancias)
		{
			Lista <Service> aux = new Lista<>();
			for (Service actual : listaJson)
			{
				double number = getDistance(lat, lon, actual.getPickupLatitud(), actual.getPickupLongitud());
				 DecimalFormat df = new DecimalFormat("#.0");
				String llave =df.format(number);
				if(llave.equalsIgnoreCase(cod) )
				{
					aux.add( actual);
				}
			}

			htSepC2.put(cod, aux);
		}
	}
	@Override
	public Lista<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		// TODO Auto-generated method stub
		Lista<TaxiConServicios> rta =  new Lista<TaxiConServicios>();
		Lista<String> listaTaxis = bst1A.get(compania);
		Lista<Service> listaServicios = htSepA1.get(zonaInicio);
		int max = 0;
		for (String idActual: listaTaxis)
		{	int cont = 0;
		for (Service servicio: listaServicios)
		{
			if(servicio.getTaxiId().equalsIgnoreCase(idActual))
			{cont++;}
		}
		if(cont>max)
		{max = cont;}
		}

		for (String idActual: listaTaxis)
		{	
			int cont2 = 0;
			TaxiConServicios taxi = new TaxiConServicios(idActual, compania);
			for (Service servicio: listaServicios)
			{
				if(servicio.getTaxiId().equalsIgnoreCase(idActual))
				{cont2++;
				taxi.agregarServicio(servicio);}
			}
			if(cont2==max)
			{rta.add(taxi);}
		}


		return rta;
	}


	@Override
	public Lista<Service> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub

		int numero = duracion/60;
		System.out.println(numero);
		Lista<Service> rta = htA2.get(numero+1);
		System.out.println(rta.size());

		return rta;
	}


	@Override
	public Lista<Service> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		Lista<Service> lista = new Lista<Service>();
		int min = (int)distanciaMinima;
		int may = (int)distanciaMaxima;
		for (int i = min ; i < (may+2); i++ )
		{
			Lista<Service> lis = new Lista<Service>();
			lis =  htDis.get(i);
			for (Service actual: lis)
			{
				if (actual.getTripMiles() > distanciaMinima&& actual.getTripMiles() <distanciaMaxima)
				{
					lista.add(actual);
				}
			}
		}


		return lista;
	}


	@Override
	public Lista<Service> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		String llave = zonaInicio+"-"+zonaFinal;
		Lista<Service> lis = new Lista<>();
		String inicio = fechaI +"T"+ horaI;
		String fin = fechaF + "T"+ horaF;
		System.out.println(htSep2B.get(llave).size());
		for (Service actual:htSep2B.get(llave))
		{
			if(actual.compareStart(inicio)>=0 && actual.compareStart(fin)<=0)
			{
				lis.add(actual);
			}
		}
		
	

		return lis;
	}


	@Override
	public TaxiConServicios[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		Lista<TaxiConServicios> listaTaxis = new Lista<TaxiConServicios>();
		for (Service servicio : listaJson)
		{
			String id = servicio.getTaxiId();
			String compania = servicio.getCompany();

			if (listaTaxis.size()==0)
			{
				TaxiConServicios taxi = new TaxiConServicios(id, compania);
				taxi.agregarServicio(servicio);
				listaTaxis.add(taxi);}
			else
			{
				Boolean encontro = false;

				for (TaxiConServicios t: listaTaxis)
				{
					if(t.getTaxiId().equalsIgnoreCase(id))
					{
						encontro = true;
						t.agregarServicio(servicio);
					}
				}

				if(!encontro)
				{
					TaxiConServicios taxi = new TaxiConServicios(id, compania);
					taxi.agregarServicio(servicio);
					listaTaxis.add(taxi);
				}
			}
		}

		TaxiConServicios[] arregloF = new TaxiConServicios[listaTaxis.size()];
		int i = 0;
		for (TaxiConServicios t: listaTaxis)
		{
			arregloF[i] = t;
			i++;
		}

		HeapSort.sort(arregloF);

		return arregloF; 
	}
	
		


	@Override
	public Lista<Service> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double longitudReq2C)
	{
		Lista<Service>res =new Lista <>();
		 DecimalFormat df = new DecimalFormat("#.0");
		String llave =df.format(longitudReq2C);
		Lista<Service> l=htSepC2.get(llave);
		for (int i =0 ;i< l.size();i++)
		{
			if (l.getByPos(i).getTaxi().getTaxiId().equals(taxiIDReq2C)){
				res.add(l.getByPos(i));
			}
				
		}
	
		
		// TODO Auto-generated method stub
		
		return res;
	}


	@Override
	public Lista<Service> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		// TODO Auto-generated method stub
		Lista<Service> listaFinal =new Lista<Service>();
		int j = 0;
		int k = 1;
		int max3C = 15;
		int m = 0;


		Lista<FechaServicios> listaFechas = new Lista<>();
		FechaServicios fecha1 = new FechaServicios(0); listaFechas.add(fecha1);
		//		FechaServicios fecha2 = new FechaServicios(15); listaFechas.add(fecha2);
		//		FechaServicios fecha3 = new FechaServicios(30); listaFechas.add(fecha3);
		//		FechaServicios fecha4 = new FechaServicios(45); listaFechas.add(fecha4);
		//		FechaServicios fecha5 = new FechaServicios(100); listaFechas.add(fecha5);
		//		FechaServicios fecha6 = new FechaServicios(115); listaFechas.add(fecha6);
		//		FechaServicios fecha7 = new FechaServicios(130); listaFechas.add(fecha7);
		//		FechaServicios fecha8 = new FechaServicios(145); listaFechas.add(fecha8);
		//		FechaServicios fecha9 = new FechaServicios(200); listaFechas.add(fecha9);
		//		FechaServicios fecha10 = new FechaServicios(215); listaFechas.add(fecha10);
		//		FechaServicios fecha11= new FechaServicios(230); listaFechas.add(fecha11);
		//		FechaServicios fecha12= new FechaServicios(245); listaFechas.add(fecha12);
		//		FechaServicios fecha13 = new FechaServicios(300); listaFechas.add(fecha13);
		//		FechaServicios fecha14 = new FechaServicios(315); listaFechas.add(fecha14);
		
		while(max3C<15000)
		{
			
				if(k<3)
				{
					FechaServicios nuevo1 = new FechaServicios(max3C);
					k++;
					max3C +=15;
					listaFechas.add(nuevo1);
												System.out.println("Se a�adio rango: " + max3C);

				}
				else
				{
					FechaServicios nuevo2 = new FechaServicios(max3C);
					k = 0;
					max3C+=55;
					listaFechas.add(nuevo2);					
												System.out.println("Se a�adio rango: " + max3C);
				}
		}

		for(Service servicio: listaJson)
		{
//			System.out.println(servicio.getTimeFromStart() + ": " + m);
			boolean encontro = false;
			//			System.out.println("Servicio" + m);
			//			System.out.println(servicio.getTimeFromStart());
			while(!encontro)
			{
				for(FechaServicios fechaActual: listaFechas)
				{
					
					if (fechaActual.estaDentro(Math.abs(servicio.getTimeFromStart())))
					{
						encontro = true;
						fechaActual.agregarServicio(servicio);
					}

					
					}
				}

			}
			m++;
		

		for( FechaServicios fActual: listaFechas)
		{
			Lista<Service> lista = fActual.getServiciosAsociados();
			bst3C.put(fActual.getHoraInicio(), fActual);
		}

		Boolean encontroAux = false;
		String remplazo = hora.replace(":", "");
		String remplazofinal = remplazo.replace(".", "");
		Double aux = Double.parseDouble(remplazofinal);
//		double resta = 90000000;
//		aux -= 20000000;
		double valor = 0;
		while(!encontroAux)

			for( FechaServicios fActual: listaFechas)
			{
				if(fActual.estaDentro(aux))
				{encontroAux = true; valor = fActual.getHoraInicio();}
			}

		FechaServicios fechaAux = bst3C.get(valor);
		listaFinal = fechaAux.getServiciosAsociados();

		System.out.println("Arbol creado");

		System.out.println("Listo");
		return listaFinal;
	}










}

