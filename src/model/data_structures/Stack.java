package model.data_structures;


public interface Stack <T extends Comparable<T>> {
	/** Agrega un nuevo elemento a la cabeza de la lista */
	public void push (T item);
	
	/** Elimina el primer elemento de la lista  
	 * @return el nuevo primer elemento, null si no existe.
	 * */
	public T pop();
	
	/** Evaluan si la lista esta vacia 
	 * 	 * @return true si esta vacia false de lo contrario. 
	 */
	public boolean isEmpty();

	/** Carcula el tamaño de la lista  
	 * 	 * @return nuemero de elementos en la lista. 
	 */
	public int size();
	
}
