package controller;

import api.ITaxiTripsManager;
import model.data_structures.Lista;
import model.logic.TaxiTripsManager;
import model.vo.Service;
import model.vo.Taxi;
import model.vo.TaxiConPuntos;
import model.vo.TaxiConServicios;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager =new TaxiTripsManager();

	//Carga El sistema
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	//1A
	public static Lista<TaxiConServicios> R1A(int zonaInicio, String compania)
	{
		return manager.A1TaxiConMasServiciosEnZonaParaCompania(zonaInicio, compania);
	}

	//2A
	public static Lista<Service> R2A(int duracion)
	{
		return manager.A2ServiciosPorDuracion(duracion);
	}

	//1B
	public static Lista<Service> R1B(double distanciaMinima, double distanciaMaxima)
	{
		return manager.B1ServiciosPorDistancia(distanciaMinima, distanciaMaxima);
	}

	//2B
	public static Lista<Service> R2B(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF)
	{
		return manager.B2ServiciosPorZonaRecogidaYLlegada(zonaInicio, zonaFinal, fechaI, fechaF, horaI, horaF);
	}	
	//1C
	public static TaxiConServicios[] R1C()
	{
		return manager.R1C_OrdenarTaxisPorPuntos();
	}	
	//2C
	public static Lista<Service> R2C(String taxiIDReq2C, double millas)
	{
		return manager.R2C_LocalizacionesGeograficas(taxiIDReq2C, millas);
	}
	//3C
	public static Lista<Service> R3C(String fecha, String hora) 
	{
		return manager.R3C_ServiciosEn15Minutos(fecha, hora);
		
	}	
	public static double distancia( double lat1, double lon1)
	{
		return manager.distanciaRefe(lat1, lon1);
	}
}
